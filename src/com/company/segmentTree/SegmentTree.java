package com.company.segmentTree;

public class SegmentTree<E> {
    private E[] tree;
    private E[] data;
    private Merger<E> merger;
    public SegmentTree(E[] arr,Merger<E> merger){
        data=(E[]) new Object[arr.length];
        this.merger=merger;
        for(int i=0;i<arr.length;i++){
            data[i]=arr[i];
        }
        tree=(E[]) new Object[4*arr.length];
        buildSegmentTree(0,0,data.length-1);
    }
    //在treeIindex位置创建从l到r区间的线段树
    private void buildSegmentTree(int treeIndex,int l,int r){
        if(l==r){
            tree[treeIndex]=data[l];
            return;
        }
        int leftTree=leftChild(treeIndex);
        int rightTree=rightChild(treeIndex);
        int mid=l+(r-l)/2;
        buildSegmentTree(leftTree,l,mid);
        buildSegmentTree(rightTree,mid+1,r);
        tree[treeIndex]=merger.merger(tree[leftTree],tree[rightTree]);
    }
    public int getSize(){
        return data.length;
    }
    public E get(int  index){
        if(index<0||index>data.length){
            throw new IllegalArgumentException("index is illegal");
        }
        return data[index];
    }
    private int leftChild(int  index){
        return 2*index+1;
    }
    private int rightChild(int  index){
        return 2*index+2;
    }
}

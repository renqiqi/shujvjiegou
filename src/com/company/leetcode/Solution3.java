package com.company.leetcode;

public class Solution3 {
    public ListNode removeElement(ListNode head,int val){
        if(head==null){
            return head;
        }
        head.next=removeElement(head.next,val);
        if(head.val==val){//如果head中的val==val的话，吧head删除，所以返回head的next
            return head.next;
        }else {//没有把head删除，返回的以head为头结点的链表
            return head;
        }

    }

}

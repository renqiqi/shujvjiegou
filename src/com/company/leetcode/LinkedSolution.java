package com.company.leetcode;

//从链表中删除值为val的所有元素
public class LinkedSolution {
    public ListNode removeElement(ListNode head ,int val){
//        while(head!=null&&head.val==val){
//            ListNode delNode =head;
//            head=head.next;
//            delNode.next=null;
//        }
//        if(head==null){
//            return head;
//        }
        ListNode dummyHead=new ListNode(-1);
        ListNode prev=dummyHead;
        while(prev.next!=null){
            if(prev.next.val==val){
                ListNode delNode=prev.next;
                prev.next=delNode.next;
                delNode.next=null;
            }else {
                prev=prev.next;
            }
        }
        return dummyHead.next;
    }
}

package com.company;

import com.company.BST.BST;

public class Main {

    public static void main(String[] args) {
        BST<Integer> bst=new BST<>();
        int[] nums={7,3,2,6,9,4};
        for(int num :nums){
            bst.add(num);
        }
        bst.preOrder();
        System.out.println("------------------------------");
        bst.inOrder();
        System.out.println("------------------------------");
        bst.postOrder();
    }
}
